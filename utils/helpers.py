def stringify_list(list_) -> str:
    """Takes a list as an input and casts the list object as a string.

    Args:
        list_ (list): Python list you want to conver to string

    Returns:
        str: string object you can convert to list using eval() funciton
    """

    if type(list_) != list:
        list_ = [list_]
    _ = ""
    for item in list_:
        _ += f"\"{str(item)}\", "
    return f"[{_[:-2]}]"
