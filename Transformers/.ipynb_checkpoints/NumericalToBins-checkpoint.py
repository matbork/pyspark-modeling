import logging
from os import getenv

from pyspark import keyword_only
from pyspark.ml import Transformer
from pyspark.sql import DataFrame
from pyspark.ml.param import TypeConverters
from pyspark.ml.param.shared import Param, Params
from pyspark.ml.util import DefaultParamsReadable, DefaultParamsWritable
from pyspark.ml.feature import Bucketizer
import pyspark.sql.functions as f
from pyspark.sql.types import *


ENCODING = getenv('ENCODING', 'utf-8')
LOG_LEVEL = getenv('LOG_LEVEL', 'DEBUG')
logging.basicConfig(level=logging.getLevelName(LOG_LEVEL), format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


class NumericalToBins(Transformer, Params, DefaultParamsReadable, DefaultParamsWritable):
    """
    Getting numerical column into bins with custom names.
    Uses Bucketizer transformer.

    :args:
        inputCol (str): input column
        outputCol (str): output column name
        splits (list): list of splits
    """

    inputCol = Param(Params._dummy(),
                    "inputCol",
                    "inputCol documentation here",
                    typeConverter=TypeConverters.toString) # e.g. TypeConverters.toList

    outputCol = Param(Params._dummy(),
                      "outputCol",
                      "outputCol doc",
                      typeConverter=TypeConverters.toString)

    splits = Param(Params._dummy(),
                   "splits",
                   "splits doc",
                   typeConverter=TypeConverters.toList)

    names_dict = Param(Params._dummy(),
                       "names_dict",
                       "names_dict doc",
                       typeConverter=None)

    @keyword_only
    def __init__(self, inputCol:str = None,
                 outputCol:str = None,
                 splits:list = None,
                 names_dict:dict = None):
        super(NumericalToBins, self).__init__()
        kwargs = self._input_kwargs
        self._set(**kwargs)

    def get_inputCol(self):
        return self.getOrDefault(self.inputCol)

    def get_outputCol(self):
        return self.getOrDefault(self.outputCol)

    def get_splits(self):
        return self.getOrDefault(self.splits)

    def get_names_dict(self):
        return self.getOrDefault(self.names_dict)

    def _transform(self, X: DataFrame) -> DataFrame:
        inputCol = self.get_inputCol()
        outputCol = self.get_outputCol()
        splits = self.get_splits()
        names_dict = self.get_names_dict()
        logging.info("NumericalToBins()")

        bucketizer = Bucketizer(splits=splits, inputCol=inputCol, outputCol=outputCol)
        
        udf_foo = f.udf(lambda x : names_dict[x], StringType())
        df_buck = bucketizer.setHandleInvalid("keep").transform(X)
        if names_dict:
            X = df_buck.withColumn(outputCol, udf_foo(outputCol))
        else:
            X = df_buck
        return X