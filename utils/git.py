import subprocess


def get_last_commit_hash() -> str:
    """Returns the hash of the most recent commit as a string

    Returns:
        str: hash of the most recent commit
    """
    
    commit_hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip().decode('ascii')
    return commit_hash
