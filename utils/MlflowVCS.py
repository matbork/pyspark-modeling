import os
import mlflow


class MlflowVCS():
    """
    This class deals with interaction between VCS and mlflow experiments.
    """
    
    
    def __init__(self, experiment_name: str, parent_run_name: str=None, previous_run_id: str=None, modeling_run_id: str=None):
        """Setup the experiment and read required variables.

        Args:
            experiment_name (str): mlflow experiment you want to refer to
            parent_run_name (str, optional): parent run you will be logging runs to
            previous_run_id (str, optional): id of the previous run in the pipeline. Defaults to None.
            modeling_run_id (str, optional): id of the final experiment
        """
        
        self.experiment_name_ = experiment_name
        mlflow.set_experiment(experiment_name)
        
        self.client_ = mlflow.tracking.client.MlflowClient()
        self.experiment_id_ = self.client_.get_experiment_by_name(experiment_name).__dict__['_experiment_id']
        
        print(f"Active experiment:\n\t\"{self.experiment_name_}\", experiment id: \"{self.experiment_id_}\"")
        
        if parent_run_name is not None:        
            self.parent_run_name_ = parent_run_name
            run_list = self.client_.search_runs(
                experiment_ids=self.experiment_id_,
                filter_string=f"tags.\"mlflow.runName\" = \"{parent_run_name}\"",
                max_results=1
            )        
            print(f"Parent run name set to:\n\t\"{self.parent_run_name_}\"")
            if len(run_list)==0:
                print("\tNo run under this name exists. Run will be created.")
            else:
                print("\tRun already exists. Any child run will be assigned to this parent run.")

        self.previous_run_id_ = previous_run_id
        if parent_run_name is not None:
            if previous_run_id is None:
                print("No previous run specified.")
            else:
                print(f"Future runs will be pointing to the run id: \"{previous_run_id}\"")
        
        self.modeling_run_id_ = modeling_run_id
        if modeling_run_id is not None:
            self.modeling_run_id_ = modeling_run_id
            self.preprocessing_run_id_ = self.get_run_data(modeling_run_id, '_tags')['preprocessing_run_id']
            print(f"Modeling run set to: \"{modeling_run_id}\"")
            print(f"Preprocessing run set to: \"{self.preprocessing_run_id_}\"")
    
    
    def parent_run_info(self) -> dict:
        """Prepare a dictionary to feed to mlflow.start_run()

        Returns:
            dict: dictionary to feed to mlflow.start_run() method
        """

        run_list = self.client_.search_runs(
            experiment_ids=self.experiment_id_,
            filter_string=f"tags.\"mlflow.runName\" = \"{self.parent_run_name_}\"",
            max_results=1
        )
        if len(run_list)==0:
            self.parent_run_id_ = None
        else:
            self.parent_run_id_ = run_list[0].__dict__['_info'].__dict__['_run_id']

        
        
        run_params = {
            'run_name':self.parent_run_name_,
            'run_id':self.parent_run_id_
        }
        return run_params
    
    
    
    @staticmethod    
    def get_run_info(run_id: str, property_: str) -> str:
        """Access desired info of a run specified in the class constructor.
        
        Possible properties:
        - '_run_uuid',
        - '_run_id',
        - '_experiment_id',
        - '_user_id', 
        - '_status',
        - '_start_time',
        - '_end_time', 
        - '_lifecycle_stage', 
        - '_artifact_uri'

        Args:
            run_id (str): desired run id
            property_ (str): property you want to access

        Returns:
            str: property value of a specific run
        """
        
        run = mlflow.get_run(run_id)
        return run.__dict__['_info'].__dict__[property_]
    
    
    
    @staticmethod
    def get_run_data(run_id: str, property_: str) -> dict:
        """Access desired data of a run specified in the class constructor.
        
        Possible properties:
        - '_metrics',
        - '_params',
        - '_tags'

        Args:
            run_id (str): desired run id
            property_ (str): property you want to access

        Returns:
            dict: dictionary returned by a specific property
        """
        
        run = mlflow.get_run(run_id)
        return run.__dict__['_data'].__dict__[property_]

    
    
    def filter_runs(self, filter_string: str=None, order_by: list=None) -> mlflow.entities.run.Run:
        """Filter runs in a an experiment (set up in the constructor) by chosen criteria.
        Returns only the first matching run. If no parameters are given, it returns the newest run.

        Args:
            filter_string (str, optional): filtering criteria. Defaults to None.
            order_by (list, optional): ordering criteria. Defaults to None.

        Returns:
            mlflow.entities.run.Run: run returned by filtering criteria
        """
        
        run = self.client_.search_runs(
            experiment_ids=self.experiment_id_,
            filter_string=filter_string,
            max_results=1,
            order_by=order_by
        )[0]
        return run
    
    
    
    @staticmethod
    def get_active_run_property(property_: str) -> str:
        """Access desired property of active run. Use only within 'mlflow.start_run()' scope.
        
        Possible properties:
        - '_run_uuid',
        - '_run_id',
        - '_experiment_id',
        - '_user_id', 
        - '_status',
        - '_start_time',
        - '_end_time', 
        - '_lifecycle_stage', 
        - '_artifact_uri'

        Args:
            property_ (str): property you want to access

        Returns:
            str: property value of active run
        """
        
        return mlflow.active_run().__dict__['_info'].__dict__[property_]
    
    

    @staticmethod
    def set_preprocessing_tags(commit_hash: str, storage_folder: str, file_list: str) -> dict:
        """Method to ensure that required tags are set when logging a preprocessing run.

        Args:
            commit_hash (str): commit hash of most recent code version
            storage_folder (str): storage path to raw data
            file_list (str): list of raw files from storage that are used to build a complete data frame

        Returns:
            dict: dictionary with required mlflow tags
        """     
        
        tags = {
            'commit_hash':commit_hash,
            'storage_folder':storage_folder,
            'file_list':file_list,
        }
        return tags
    
    
    
    def set_modeling_tags(self, commit_hash: str) -> dict:
        """Method to ensure that required tags are set when logging a modeling run.

        Args:
            commit_hash (str): commit hash of most recent code version

        Returns:
            dict: dictionary with required mlflow tags
        """
        
        tags = {
            'commit_hash':commit_hash,
            'preprocessing_run_id':self.previous_run_id_
        }
        return tags
    


    @staticmethod
    def read_file_list(run_id: str) -> list:
        """Get a Python list from a 'file_list' tag of a preprocessing run

        Args:
            run_id (str): id of a preprocessing run

        Returns:
            list: list containing used files
        """
        
        run = mlflow.get_run(run_id)
        file_list_str = run.__dict__['_data'].__dict__['_tags']['file_list']
        return eval(file_list_str)
    
    
    
    def get_model_path(self) -> str:
        """Get the path to a model on artifact storage

        Returns:
            str: path to the model
        """
        
        return self.get_run_info(self.modeling_run_id_, '_artifact_uri') + "/model/model.pkl"
    
    
    
    def get_pipeline_path(self) -> str:
        """Get the path to a pipeline on artifact storage

        Returns:
            str: path to the pipeline
        """
        
        return self.get_run_info(self.preprocessing_run_id_, '_artifact_uri') + "/pipeline/model.pkl"

    
    
    def load_raw_file_paths(self) -> list:
        """Get a list of complete path to raw files on the original storage

        Returns:
            list: list of file paths
        """
        
        file_list = self.read_file_list(self.preprocessing_run_id_)
        storage_path = self.get_run_data(self.preprocessing_run_id_, '_tags')['storage_folder']
        file_path_list = []
        for file in file_list:
            file_path_list.append(storage_path+file)
            
        return file_path_list
