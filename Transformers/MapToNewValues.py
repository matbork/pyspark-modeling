import logging
from os import getenv

from pyspark import keyword_only
from pyspark.ml import Transformer
from pyspark.sql import DataFrame
from pyspark.ml.param import TypeConverters
from pyspark.ml.param.shared import Param, Params
from pyspark.ml.util import DefaultParamsReadable, DefaultParamsWritable
import pyspark.sql.functions as f


ENCODING = getenv('ENCODING', 'utf-8')
LOG_LEVEL = getenv('LOG_LEVEL', 'INFO')
logging.basicConfig(level=logging.getLevelName(LOG_LEVEL), format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


class MapToNewValues(Transformer, Params, DefaultParamsReadable, DefaultParamsWritable):
    """
        Map values by dictionary.
    Set numerical_coloumn as True to get Integer column back.
    !WARN! values in map_dict should be strings.

    :args:
        column (str): column on which we should do mapping
        map_dict (dict): dictionary, mapping is based on this e.g.
            {"val1": "val2", "val3": "val4"}
        numerical_column (boolean): if True then mapped values are Integers
    """

    # defining params in here
    column = Param(Params._dummy(),
                    "column",
                    "column documentation here",
                    typeConverter=None) # e.g. TypeConverters.toList

    map_dict = Param(Params._dummy(),
                     "map_dict",
                     "map_dict documentation",
                     typeConverter=None)

    numerical_column_boolean = Param(Params._dummy(),
                                    "numerical_column_boolean",
                                     "numerical_column_boolean documentation",
                                     typeConverter=TypeConverters.toBoolean)

    @keyword_only
    def __init__(self, column:str = None,
                       map_dict:dict = None,
                       numerical_column_boolean:bool = False):
        super(MapToNewValues, self).__init__()
        kwargs = self._input_kwargs
        self._set(**kwargs)

    def get_column(self):
        return self.getOrDefault(self.column)

    def get_map_dict(self):
        return self.getOrDefault(self.map_dict)

    def get_numerical_column_boolean(self):
        return self.getOrDefault(self.numerical_column_boolean)

    def _transform(self, X: DataFrame) -> DataFrame:
        logging.info("MapToNewValues() on column {}".format(self.column))

        column = self.get_column()
        map_dict = self.get_map_dict()
        numerical_column_boolean = self.get_numerical_column_boolean()

        X = X.replace(to_replace=map_dict, subset=[column])
        if numerical_column_boolean:
            X = X.withColumn(column, f.col(column).cast("Integer"))
        return X