import logging
from os import getenv

from pyspark import keyword_only
from pyspark.ml import Transformer
from pyspark.sql import DataFrame
from pyspark.ml.param import TypeConverters
from pyspark.ml.param.shared import Param, Params
from pyspark.ml.util import DefaultParamsReadable, DefaultParamsWritable


ENCODING = getenv('ENCODING', 'utf-8')
LOG_LEVEL = getenv('LOG_LEVEL', 'INFO')
logging.basicConfig(level=logging.getLevelName(LOG_LEVEL), format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


class DropColumns(Transformer, Params, DefaultParamsReadable, DefaultParamsWritable):
    # defining params in here
    columns_to_drop = Param(Params._dummy(),
                    "columns_to_drop",
                    "columns_to_drop documentation here",
                    typeConverter=TypeConverters.toList) # e.g. TypeConverters.toList

    @keyword_only
    def __init__(self, columns_to_drop=None):
        super(DropColumns, self).__init__()
        kwargs = self._input_kwargs
        self._set(**kwargs)

    def get_columns_to_drop(self):
        return self.getOrDefault(self.columns_to_drop)

    def _transform(self, X: DataFrame) -> DataFrame:
        columns_to_drop = self.get_columns_to_drop()
        logging.info("DropColumnsTest() on {} columns".format(columns_to_drop))
        X = X.drop(*[c for c in columns_to_drop])
        return X


